package com.example.bottomnavigationviewexample;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import com.google.android.material.bottomnavigation.BottomNavigationView;

public class MainActivity extends AppCompatActivity {

    ImageView imgMap;
    ImageView imgDial;
    ImageView imgMail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        imgMap = (ImageView) findViewById(R.id.imgMap);
        imgDial = (ImageView) findViewById(R.id.imgDial);
        imgMail = (ImageView) findViewById(R.id.imgMail);

        BottomNavigationView bottomNavigationView = (BottomNavigationView)
                findViewById(R.id.bottomNavigationView);

        bottomNavigationView.setOnNavigationItemSelectedListener(
                new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.action_map:
                                imgMap.setVisibility(View.VISIBLE);
                                imgDial.setVisibility(View.GONE);
                                imgMail.setVisibility(View.GONE);
                                break;
                            case R.id.action_dial:
                                imgMap.setVisibility(View.GONE);
                                imgDial.setVisibility(View.VISIBLE);
                                imgMail.setVisibility(View.GONE);
                                break;
                            case R.id.action_mail:
                                imgMap.setVisibility(View.GONE);
                                imgDial.setVisibility(View.GONE);
                                imgMail.setVisibility(View.VISIBLE);
                                break;
                        }
                        return true;
                    }
                });
    }
}